interface Bank{
    public void withdrawMoney(double amount);
    public void depositMoney(double amount);
}
class Account implements Bank{
    private double currentAccountBalance;
    private int accountNumber;

    public Account(double currentAccountBalance,int accountNumber){
        this.currentAccountBalance = currentAccountBalance;
        this.accountNumber = accountNumber;
    }
    @Override
    public void withdrawMoney(double amount) {
        if(currentAccountBalance>=amount) {
            currentAccountBalance -= amount;
            System.out.println("Amount withdrawn: "+amount);
            System.out.println("New Account Balance: "+currentAccountBalance);
        }
        else{
            System.out.println("Insufficient Funds");
        }
    }

    @Override
    public void depositMoney(double amount) {
        currentAccountBalance+= amount;
        System.out.println("Amount deposited: "+amount);
        System.out.println("New Account Balance: "+currentAccountBalance);
    }
}
class CreditCardAccount implements Bank{
    double currentCreditLimit;
    int creditCardNumber;

    public CreditCardAccount(double currentCreditLimit,int creditCardNumber){
        this.currentCreditLimit = currentCreditLimit;
        this.creditCardNumber = creditCardNumber;
    }
    @Override
    public void withdrawMoney(double amount) {
        if(currentCreditLimit>=amount){
            currentCreditLimit -= amount + (0.005 * amount);
            System.out.println("Amount withdrawn:"+amount);
            System.out.println("New Credit limit:"+currentCreditLimit);
        }
        else{
            System.out.println("Credit limit exceeded.");
        }

    }

    @Override
    public void depositMoney(double amount) {
        currentCreditLimit += amount;
        System.out.println("Amount deposited: "+amount);
        System.out.println("New Credit limit:"+currentCreditLimit);

    }
}
public class Main2{
    public static void main(String[] args) {
      Account acc1 = new Account(10000,177610025);
      CreditCardAccount cc1 = new CreditCardAccount(50000,254877900);
        acc1.withdrawMoney(500);
        acc1.depositMoney(1000);

        cc1.withdrawMoney(2000);
        cc1.depositMoney(25000);
    }

}
