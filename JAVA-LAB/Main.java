
import java.util.*;

class Box{
    double length;
    double breadth;
    //double height;
    public void printValues() {
        System.out.println("Length: " + length);
        System.out.println("Breadth: " + breadth);
    }
}
class Rectangle extends Box{
    Rectangle(double length, double breadth){
        this.length = length;
        this.breadth = breadth;
    }
    public void computeArea(){
        double a = length * breadth;
        System.out.println("Area of Rectangle is: "+a);
    }
    public void computePerimeter(){
        double b = 2*(length + breadth);
        System.out.println("Perimeter of Rectangle is: "+b);
    }
}

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the length of rectangle: ");
        double length = input.nextDouble();
        System.out.println("Enter the breadth of rectangle: ");
        double breadth = input.nextDouble();
        Rectangle R = new Rectangle(length, breadth);
        R.printValues();
        R.computePerimeter();
        R.computeArea();
    }
}
