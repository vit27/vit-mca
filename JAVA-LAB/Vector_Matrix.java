import java.util.Scanner;

interface VectorOrMatrices{
    abstract public void addition();
    abstract public void multiplication();
}

class Vector implements VectorOrMatrices{

    int a,b,c;
    public Vector(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public Vector(){
        return;
    }
    public void addition(){
        return;
    }
    public void multiplication(){
        return;
    }
    public Vector addition(Vector v1, Vector v2){
        Vector v3 = new Vector();
        v3.a = v1.a + v2.a;
        v3.b = v1.b + v2.b;
        v3.c = v1.c + v2.c;

        return v3;
    }

    public int multiplication(Vector v1, Vector v2){
        return (v1.a*v2.a + v1.b*v2.b + v1.c*v2.c);
    }
}

class Matrix implements VectorOrMatrices{

    public Matrix(){
        return;
    }
    public void addition(){
        return;
    }
    public void multiplication(){
        return;
    }

    void displayMatrix(int[][] A,int row, int col){
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                System.out.print(A[i][j]+" ");
            }
            System.out.println();
        }
    }
    public void addition(int Array1[][],  int Array2[][], int m, int n, int x, int y ){
        
        if(m!=x && n!=y){
            System.out.println("Matrix Addition not possible!");
        }
        else{
            int Array3[][] = new int[m][n];

            for(int i=0; i<m; i++){
                for(int j=0; j<n; j++){
                    Array3[i][j] = Array1[i][j] + Array2[i][j];
                }
            }
            displayMatrix(Array3, m, n);
        }
    }
    public void multiplication(int A1[][], int A2[][],int m, int n, int x, int y){
        if(n!=x){
            System.out.println("Matrix Multiplication not possible!");
            return;
        }
        else{
            int [][] A3 = new int [m][y];

            for(int i=0; i<m; i++){
                for(int j = 0; j<y; j++){
                    A3[i][j] = 0;
                    for(int k = 0; k<m; k++){
                        A3[i][j] += A1[i][k]*A2[k][j];
                    }

                }
            }
            displayMatrix(A3, m, y);
        }
        
    }
}
public class Main {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        Vector v1 = new Vector(12,10,11);
        System.out.println("Vector v1: "+v1.a+"i + "+v1.b+"j + "+v1.c+"k");

        Vector v2 = new Vector(14,19,17);
        System.out.println("Vector v2: "+v2.a+"i + "+v2.b+"j + "+v2.c+"k");

        Vector v3 = new Vector();
        System.out.println("Addition of both Vectors: ");
        v3 = v3.addition(v1,v2);
        System.out.println("v3: "+v3.a+"i + "+v3.b+"j + "+v3.c+"k");

        int result = v3.multiplication(v1,v2);
        System.out.println("Scalar Product of both Vectors: "+result);

        Matrix m1 = new Matrix();

        System.out.print("\nEnter no. of rows for 1st Matrix: ");
        int m = sc.nextInt();
        System.out.print("Enter no. of columns for 1st Matrix: ");
        int n = sc.nextInt();
        int Array1[][] = new int[m][n];

        System.out.println("Enter Array elements: ");
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
              Array1[i][j] = sc.nextInt();
            }
        }
        System.out.println("First Matrix Elements: ");
        m1.displayMatrix(Array1,m,n);

        System.out.print("Enter no. of rows for 2nd Matrix: ");
        int x = sc.nextInt();
        System.out.print("Enter no. of columns for 2nd Matrix: ");
        int y = sc.nextInt();
        int Array2[][] = new int[x][y];

        System.out.println("Enter Array elements: ");
        for(int i=0; i<x; i++){
            for(int j=0; j<y; j++){
              Array2[i][j] = sc.nextInt();
            }
        }
        System.out.println("Second Matrix Elements: ");
        m1.displayMatrix(Array2,x,y);

        System.out.println("\nAddition of Both Matrices: ");
        m1.addition(Array1, Array2, m, n, x, y);
        System.out.println("\nMultiplication of Both Matrices: ");
        m1.multiplication(Array1, Array2,m,n,x,y);
    }
}
