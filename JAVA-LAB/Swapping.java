class Example{
    public int x,y;
    void swap(Example st){
        int temp = st.x;
        st.x = st.y;
        st.y = temp;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object is destroyed.");
    }
}

public class JavaMain {

    public static void main(String[] args) {
       Example e1 = new Example();
       e1.x = 10;
       e1.y = 20;
        System.out.println("Before Swapping: ");
        System.out.println("x: "+e1.x+", y: "+e1.y);
       e1.swap(e1);
        System.out.println("After Swapping: ");
       System.out.println("x: "+e1.x+", y: "+e1.y);


    }
}


