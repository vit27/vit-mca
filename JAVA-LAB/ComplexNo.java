import java.util.Scanner;
public class Complex {
    private float realPart;
    private float imagPart;

    public Complex(float realPart,float imagPart){
        this.realPart = realPart;
        this.imagPart = imagPart;
    }

    public Complex(){
        return;
    }

    public static Complex AddComplex(Complex c1, Complex c2){

        Complex c3 = new Complex();
        c3.realPart = c1.realPart + c2.realPart;
        c3.imagPart = c1.imagPart + c2.imagPart;

        return c3;
    }

    public static Complex SubtractComplex(Complex c1, Complex c2){
        Complex c3 = new Complex();
        c3.realPart = c1.realPart - c2.realPart;
        c3.imagPart = c1.imagPart - c2.imagPart;

        return c3;
    }

    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter real part of First Complex Number: ");
        float real1 = sc.nextFloat();
        System.out.print("Enter imaginary part of First Complex Number: ");
        float imag1 = sc.nextFloat();

        Complex c1 = new Complex(real1,imag1);
        
        System.out.println("First Complex no(c1): ("+c1.realPart+","+c1.imagPart+")");

        System.out.print("Enter real part of Second Complex Number: ");
        float real2 = sc.nextFloat();
        System.out.print("Enter imaginary part of Second Complex Number: ");
        float imag2 = sc.nextFloat();

        Complex c2 = new Complex(real2,imag2);

        System.out.println("Second Complex no(c2): ("+c2.realPart+","+c2.imagPart+")");
        
        Complex c3;
        System.out.print("Addition of c1 and c2: ");
        c3 = AddComplex(c1,c2);
        System.out.println("("+c3.realPart+","+c3.imagPart+")");

        System.out.print("Subtraction of c1 and c2: ");
        c3 = SubtractComplex(c1,c2);
        System.out.println("("+c3.realPart+","+c3.imagPart+")");
    }
}
