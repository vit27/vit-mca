class Species {
    static int age;
}
class Human extends Species{
    Human(int age){
        Human.age = age;
    }
    
}

public class MainJava{
    public static void main(String[] args) {
        System.out.println();
        Human h1 = new Human(25);
//        h1.age = 10;
        System.out.println("Age: " +h1.age);
    }

}