abstract class Worker{
    private String name;
    private String address;

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
    public Worker(String name, String address){
        this.name = name;
        this.address = address;
        System.out.println("Name: "+name+"\nAddress: "+address);
    }
    abstract double earnings();
}
class PieceWorker extends Worker{
    private double wage;
    private int pieces;
    public PieceWorker(double wage, int pieces){
        super("Ravi Patel","Ranchi, Jharkhand");
        this.wage = wage;
        this.pieces = pieces;
        System.out.println("Wage per piece: "+wage);
        System.out.println("Number of pieces produced:"+pieces);
    }
    public double earnings(){
        return wage * pieces;
    }
}
public class Main1 {
    public static void main(String[] args) {
        Worker p1 = new PieceWorker(45,20);
        System.out.println("Total Earning: "+p1.earnings());
    }
}