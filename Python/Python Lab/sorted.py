>>> d = {"a": 1, "b": 2, "c": 3}
>>> sorted(d, key=lambda x: x[1])
[('a', 1), ('b', 2), ('c', 3)]


>>> d = {"a": 1, "b": 2, "c": 3}
>>> sorted(d, key=lambda x: x[0])
[('a', 1), ('b', 2), ('c', 3)]


>>> d = {"a": 1, "b": 2, "c": 3}
>>> sorted(d.items(), key=lambda x: x[1])
[('a', 1), ('b', 2), ('c', 3)]

>>> d = {"a": 1, "b": 2, "c": 3}
>>> sorted(d, key=lambda x: x[1], reverse=True)
[('c', 3), ('b', 2), ('a', 1)]


