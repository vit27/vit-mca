import re

def validate_vit_email(email):
    # Check if the email address contains the @ symbol.
    if "@" not in email:
        return False

    # Check if the email address contains the dot symbol.
    if "." not in email:
        return False

    # Check if the email address contains a space symbol.
    if " " in email:
        return False

    # Check if the email address starts with vit.ac.in.
    if not email.endswith("vit.ac.in"):
        return False

    # Check if the email address contains more than 10 characters after the @ symbol.
    if len(email.split("@")[1]) > 9:
        return False

    return True

# if __name__ == "__main__":
email = input("Enter your VIT email address: ")
if validate_vit_email(email):
    print("The email address is valid.")
else:
    print("The email address is invalid.")
