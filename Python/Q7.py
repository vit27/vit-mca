weight = float(input("Enter the weight of Person in pounds: "))
height = float(input("Enter the height of Person in inches: "))

weightInKg = weight * 0.4536
heightInMeter = height * 0.0254

BMI = (weightInKg)/(heightInMeter * heightInMeter)

roundBMI = format(BMI,".2f")
print("BMI of the person is: ",roundBMI)