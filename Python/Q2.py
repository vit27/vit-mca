day = int(input("Enter the day: "))
month = int(input("Enter the month: "))
year = int(input("Enter the year: "))

currentYear = int(input("Enter the current year: "))

if currentYear < year:
    print("Invalid input")
else:
    noOfBirthday = currentYear - year

    if day!=29 and month!=2:
        print("Number of birthdays celebrated: ",noOfBirthday)
    else:
        noOfBirthday = 0
        for i in range(year + 1, currentYear + 1):
            if i%4 == 0 and i%100!=0 or i%400 == 0:
                noOfBirthday += 1

        print("Number of birthdays celebrated: ",noOfBirthday)

    