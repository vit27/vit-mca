t = int(input("Enter total no. of nuts and bolts: "))
n = int(input("Enter number of nuts: "))

perOfDefNuts = int(input("Enter % of defective nuts: "))
perOfDefBolts = int(input("Enter % of defective bolts: "))

b = t - n

defNuts = n * perOfDefNuts/100
defBolts = b * perOfDefBolts/100

totalDefItems = defNuts + defBolts

perOfDefItems = (totalDefItems/t)*100
perOfNonDefItems = format(100 - perOfDefItems,".2f")

print("Percentage of non-defective items: ", perOfNonDefItems)
